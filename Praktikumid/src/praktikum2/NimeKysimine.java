package praktikum2;

import lib.TextIO;

public class NimeKysimine {
	public static void main(String[] args) {
		String kasutajanimi;
		int nimePikkus;

		System.out.println("Mis Teie nimi on?");
		kasutajanimi = TextIO.getln();
		nimePikkus = kasutajanimi.length();
		System.out.print("Teie nimes on ");
		System.out.print(nimePikkus);
		System.out.println(" tähte");
		System.out.print("Kena päeva!");
	}
}