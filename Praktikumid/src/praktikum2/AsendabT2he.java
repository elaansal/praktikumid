package praktikum2;

import lib.TextIO;

public class AsendabT2he {
	public static void main(String[] args) {
		boolean jatka = true;
		while (jatka) {
			String a = "teie arvu";
			String b = "teie sisestatud";
			String str = null;
			System.out.println("See programm asendab " + a + " tähe " + b + " tähega");
			System.out.println("Kirjutage oma üks täht, mida soovite muuta!");
			a = TextIO.getlnString();
			while (a.length() > 1) {
				System.out.println("Sisesta ainult 1 täht!");
				a = TextIO.getlnString();
			}
			System.out.println("Kirjutage oma üks täht, MILLISEKS soovite muuta!");

			b = TextIO.getlnString();

			while (b.length() > 1) {
				System.out.println("Sisesta ainult 1 täht!");
				b = TextIO.getlnString();
			}
			System.out.println("Kirjutage oma lause");
			str = TextIO.getln();
			String lause = (str.replace(a, b));
			System.out.println(lause);
			System.out.println("Soovite uuesti mängida?");
			jatka = TextIO.getlnBoolean();
		}
	}
}
