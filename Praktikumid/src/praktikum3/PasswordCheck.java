package praktikum3;

import lib.TextIO;

public class PasswordCheck {
	public static void main(String[] args) {
		boolean uuesti = true;
		String pw = "password";
		String kpw;
		while (uuesti) {
			// Väga paljudel inimestel on parooliks "password"
			System.out.println("Kirjutage palun oma parool!");
			kpw = TextIO.getlnString();
			if (kpw.equals(pw)) {
				System.out.println("Teie parool on korrektne ja me jätkame");
				break;
			} else {
				System.out.println("Wrong password!");
			}
			if (!(kpw.equals(pw))){uuesti=true;}
		}
	}
}
