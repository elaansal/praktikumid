package praktikum3;

import lib.TextIO;

public class Tehisintellekt {
	public static void main(String[] args) {

		int age1;
		int age2;

		String kr6be = "No kurat, see vanusevahe on veits liiga suur imo";
		String kr6be2 = "Üks käis teist juba koolis, kui teine polnud sündinudki";
		String sobib = "See koosseis käib kah!";
		System.out.println("Te olete paarike jah? Kirjutage oma kaaslase või enda vanus ");
		age1 = TextIO.getlnInt();
		System.out.println("Kirjutage nüüd teine vanus ");
		age2 = TextIO.getlnInt();
		int vordus = Math.abs(age1 - age2);

		if (vordus <= 5) {
			System.out.println(sobib);
		} else if (vordus > 5 && vordus <= 10) {
			System.out.println(kr6be);
		} else if (vordus > 10) {
			System.out.println(kr6be2);
					}
		
	}

}
