package praktikum5;

import lib.TextIO;

public class Kullkiri {
	public static void main(String[] args) {

		inimestearv();

	}

	// See meetod küsib kasutajalt sisendi ja kontrollib, kas see on vahemikus
	public static int kasutajaSisestus(int min, int max) {

		System.out.println("Kirjuta oma arv");

		String lause = "";
		System.out.println(lause);
		int sisendarv = 0;
		while (true) {
			sisendarv = TextIO.getInt();
			if (sisendarv >= min && sisendarv <= max) {

				return sisendarv;

			} else {
				sisendarv = 0;
				System.out.println(lause);
			}

		}

	}

	// Meetod tekitab randomilt kulli või kirja
	public static int arvutilahend() {
		int kiri = 1;
		int kull = 0;
		if (Math.random() < 0.5) {
			return kull;
		}

		else {
			return kiri;
		}
	}

	// See meetod võrdleb kasutja pakutud lahendust ja Math.randomit
	public static int kontroll(Object kasutajaSisestus, Object kullkiri) {
		if (kasutajaSisestus == kullkiri) {
			System.out.println("Õige");
			return 1;
		} else {
			System.out.println("Pakkusid valesti");
			return 0;
		}
	}

	//See meetod küsib inimeste arvu
	public static int inimestearv() {
		System.out.println("Kirjuta inimeste arv");
		
		 int arv = TextIO.getInt();
		while (arv<1){
			System.out.println("Kirjuta uuesti inimeste arv!");
			 arv = TextIO.getInt();	  
						}
		return arv;
	}
	
//	public static int liisk(Object inimestearv){
//		int min = 1;
//		int inimarv = inimestearv();
//		int (min, inimarv){
//			int range = inimarv;
//					  return (int)(Math.random() * range);
//		}
//		
//
//		
//	}
}
