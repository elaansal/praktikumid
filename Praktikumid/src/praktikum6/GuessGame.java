package praktikum6;

import lib.TextIO;

public class GuessGame {
	public static void main(String[] args) {
		playGame();
		System.out.println("Tehtud!");
	}


	public static int ComputerNr(int min, int max) {
		int vahemik = max - min + 1;
		int arvutiarv = min + ((int) (Math.random() * vahemik));
		return arvutiarv;
	}

	static void playGame() {
		int ComputerNumber = ComputerNr(2, 14);
		int UserGuess;

		System.out.println("Arvuti valib nr teie etteantud vahemikus");
		System.out.println("Kirjuta oma pakkumine");

		while (true) {
			UserGuess = TextIO.getInt();
			if (UserGuess == ComputerNumber) {
				System.out.println("Minu arv oli " + ComputerNumber);
				break;
			}
			if (UserGuess < ComputerNumber) {
				System.out.println("Arvutiarv on suurem");
			} else if (UserGuess > ComputerNumber) {
				System.out.println("Arvutiarv on väiksem");
			}
			System.out.println();
		}

	}
}
